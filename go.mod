module bitbucket.org/Shishkin_Alex/go-cubby

go 1.15

require (
	cloud.google.com/go/storage v1.14.0
	github.com/alecthomas/participle v0.4.1
	github.com/appleboy/gin-jwt/v2 v2.6.3
	github.com/davecgh/go-spew v1.1.1
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-gonic/gin v1.5.0
	github.com/go-pg/pg/v9 v9.1.1
	github.com/joho/godotenv v1.3.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)
