package api

import (
	handler "bitbucket.org/Shishkin_Alex/go-cubby/handlers"
	"fmt"
	"log"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func RunApi() {
	r := gin.Default()
	config := cors.DefaultConfig()
	config.AllowHeaders = []string{"Origin", "Authorization", "Content-Length", "Content-Type"}
	config.AllowAllOrigins = true
	config.ExposeHeaders = []string{"Content-Length", "Content-Type", "Access-Control-Allow-Origin"}
	config.AllowCredentials = true
	r.Use(cors.New(config))
	authMiddleware, err := jwt.New(handler.JWT)
	if err != nil {
		log.Fatal("JWT Error:" + err.Error())
	}
	api := r.Group("/api")
	v1 := api.Group("/v1")
	{
		auth := v1.Group("/auth")
		{
			auth.POST("/login", authMiddleware.LoginHandler)
			auth.GET("/refresh", authMiddleware.RefreshHandler)
		}
		v1.GET("/users", authMiddleware.MiddlewareFunc(), handler.GetUsers)
		v1.GET("/current_user", authMiddleware.MiddlewareFunc(), handler.GetCurrentUser)
		olap := v1.Group("/olap")
		olap.Use(authMiddleware.MiddlewareFunc())
		{
			olap.GET("/cubeview", handler.GetCubeview)

			olap.GET("/cubes", handler.GetCube)
			olap.POST("/cubes", handler.AddCube)
			olap.DELETE("/cubes", handler.DeleteCube)
			olap.GET("/dimensions", handler.GetDimension)
			olap.POST("/dimensions", handler.AddDimension)
			olap.PUT("/dimensions/:name", handler.UpdateDimension)
			olap.DELETE("/dimensions/:name", handler.DeleteDimension)

			olap.POST("/attrs/:dimension", handler.AddAttribute)
			olap.PUT("/attrs/:dimension", handler.UpdateAttribute)

			olap.GET("/subset/:dimension/:subset", handler.GetSubset)
			olap.PUT("/subset/:dimension/:subset", handler.UpdateSubset)
			olap.POST("/subsets/:dimension", handler.AddSubset)
			olap.DELETE("/subsets/:dimension/:subset", handler.DeleteSubset)
			olap.PUT("/dbrw", handler.UpdateDBRW)
			olap.GET("/save", handler.SaveBackup)
			olap.GET("/load", handler.LoadBackup)
		}
		chart := v1.Group("/chart")
		chart.Use(authMiddleware.MiddlewareFunc())
		{
			chart.GET("/:cube", handler.GetChartData)
		}
		forms := v1.Group("/forms")
		forms.Use(authMiddleware.MiddlewareFunc())
		{
			forms.GET("", handler.GetForm)
			forms.POST("", handler.AddForm)
			forms.PUT("/:name", handler.UpdateForm)
			forms.POST("/:name", handler.GetFormWithInput)
			forms.DELETE("", handler.DeleteForm)
			// if we will use formulas on front
			//forms.PUT("/ti", handler.UpdateButtons)
			//forms.POST("/dbrw", handler.GetDBRW)

			//forms.GET("/attrs", handler.GetAttr)
			//forms.GET("/subnm", handler.SUBNM)
			//forms.POST("/multipleDBRW", handler.MultipleDBRW)
		}

		views := v1.Group("/views")
		views.Use(authMiddleware.MiddlewareFunc())
		{
			views.GET("", handler.ListViews)
			views.POST("", handler.AddView)

			view := views.Group("/:name")
			{
				view.GET("", handler.GetView)
				view.PUT("", handler.UpdateView)
				view.DELETE("", handler.DeleteView)
			}
		}

		ti := v1.Group("/integrator")
		ti.Use(authMiddleware.MiddlewareFunc())
		{
			ti.GET("/", handler.GetProcesses)
			ti.POST("/", handler.AddProcess)
			process := ti.Group("/:process")
			{
				process.GET("/run", handler.RunProcess)
				process.POST("/run", handler.RunProcessWithParams)
				process.GET("/files", handler.GetProcessList)
				process.GET("/file/:filename", handler.GetProcessFileBody)
				process.POST("/file", handler.AddProcessFile)
				process.DELETE("/file/:filename", handler.DeleteProcessFile)
				process.PUT("/file/:filename", handler.UpdateProcessFileBody)
			}

		}
		rules := v1.Group("/rules")
		rules.Use(authMiddleware.MiddlewareFunc())
		{
			rules.GET("/:cube", handler.GetRules)
			rules.PUT("/:cube", handler.UpdateRules)
		}
	}
	gin.SetMode(gin.DebugMode)
	fmt.Println("running")
	err = r.Run(":8080")
	if err != nil {
		fmt.Println(err)
	}
}
