package main

import (
	"bitbucket.org/Shishkin_Alex/go-cubby/api"
	"bitbucket.org/Shishkin_Alex/go-cubby/internal/forms"
	"bitbucket.org/Shishkin_Alex/go-cubby/internal/olap"
	"fmt"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load(".env.development")
	if err != nil {
		fmt.Println("Error loading .env file")
	}
	err = forms.LoadBackup()
	if err != nil {
		fmt.Println("Error loading forms backup")
	}
	err = olap.LoadBackup()
	if err != nil {
		fmt.Println(err)
	}

	api.RunApi()
}
