package cubeview

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

type View struct {
	Name            string   `json:"name"`
	Cube            string   `json:"cube"`
	AxisX           string   `json:"axis_x"`
	AxisY           string   `json:"axis_y"`
	DimensionValues []string `json:"dimension_values"`
}

var Views []View

func AddView(v View) {
	Views = append(Views, v)
}

func UpdateView(v View) {
	for i := range Views {
		if Views[i].Name == v.Name {
			Views[i] = v
		}
	}
}

func DeleteViewByName(name string) {
	for i := range Views {
		if Views[i].Name == name {
			Views[i] = Views[len(Views)-1]
			Views[len(Views)-1] = View{}
			Views = Views[:len(Views)-1]
		}
	}
}

func GetView(name string) View {
	for i := range Views {
		if Views[i].Name == name {
			return Views[i]
		}
	}
	return View{}
}

func save(fileName string, v interface{}) error {
	js, err := json.Marshal(v)
	if err != nil {
		return err
	}
	d1 := js
	err = ioutil.WriteFile(fileName, d1, 0644)
	if err != nil {
		return err
	}
	return nil
}

func SaveBackup() error {
	pwd, _ := os.Getwd()
	backupPath := filepath.Join(pwd, "backup")
	err := save(filepath.Join(backupPath, "views.json"), Views)
	if err != nil {
		fmt.Println(err)
		return errors.New("failed to save file")
	}
	return nil
}

func LoadBackup() error {
	pwd, _ := os.Getwd()
	backupViews, err := ioutil.ReadFile(pwd + "/backup/views.json")
	if err != nil {
		emptyFile, _ := os.Create(pwd + "/backup/views.json")
		_ = emptyFile.Close()
	}
	var savedViews []View
	err = json.Unmarshal(backupViews, &savedViews)
	if err != nil {
		return err
	}
	for _, f := range savedViews {
		newF := f
		Views = append(Views, newF)
	}
	return nil
}
