package olap

type Rule struct {
	Dimensions []*CubeRef `"["(@@ ("," @@)*)?"]"`
	RuleBody   *TypeBody  `"=" @@`
}

type CubeRef struct {
	Dimension string `(@String ":")?`
	Name      string "@String"
}
type TypeBody struct {
	Index int
	Type  string    `(@Ident ":" )?`
	Body  *Addition `@@`
}
type Addition struct {
	Multiplication *Multiplication `@@`
	Op             string          `[ @( "-" | "+" )`
	Next           *Addition       `  @@ ]`
}

type Multiplication struct {
	Body *Body           `@@`
	Op   string          `[ @( "/" | "*" | "^" )`
	Next *Multiplication `  @@ ]`
}

type Body struct {
	Index   int
	Command *Commands `@@`
	Value   *Value    `| @@`
}

type Commands struct {
	Index    int
	If       *If       "@@"
	NumToStr *NumToStr "|@@"
	Attrs    *Attrs    "|@@"
}
type Attrs struct {
	Index int
	Args  []*Addition `"ATTRS""(" ( @@ ( "," @@ )* )? ")"`
}
type NumToStr struct {
	Index int
	Args  []*Addition `"numbertostring""(" ( @@ ( "," @@ )* )? ")"`
}

type If struct {
	Index          int
	ConditionLeft  *Addition `"IF""(" @@ `
	ConditionOp    *string   `@(">" | "<" | ">""=" | "<""=" | "=""=" | "!""=")*`
	ConditionRight *Addition `@@ ","`
	ValLeft        *Addition `@@ ","`
	ValRight       *Addition `@@ ")"`
}
type DB struct {
	Index int
	Args  []*TypeBody `"DB""(" ( @@ ( "," @@ )* )? ")"`
}
type Value struct {
	Index         int
	DBRef         *DB        `@@`
	Dimension     *string    `| "$" @String`
	CubeRef       []*CubeRef `| "["(@@ ("," @@)*)?"]"`
	String        *string    `| @String`
	Int           *int       `| @Int`
	Number        *float64   `| @Float`
	Subexpression *Addition  `| "(" @@ ")"`
}
