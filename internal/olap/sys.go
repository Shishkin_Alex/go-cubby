package olap

type SecurityAccessType int

const (
	None SecurityAccessType = iota
	Read
	Write
	ReadWrite
)

type UserRoleType int

const (
	GuestRole UserRoleType = iota
	AdminRole
)
