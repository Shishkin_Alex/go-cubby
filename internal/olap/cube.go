package olap

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

var Cubes []*Cube

type Cube struct {
	Name                string
	Dimensions          []string
	DetailedValues      Storage `json: "-"`
	ConsolidatedStorage Storage `json: "-"`
	Rules               []Rule
}

type Storage struct {
	sync.Mutex
	Storage map[string]interface{}
}

func AllPossibleCases(arr [][]string) []string {
	if len(arr) == 1 {
		return arr[0]
	} else {
		var result []string
		var allCasesOfRest = AllPossibleCases(arr[1:])
		for i := 0; i < len(allCasesOfRest); i++ {
			for j := 0; j < len(arr[0]); j++ {
				result = append(result, arr[0][j]+",,"+allCasesOfRest[i])
			}
		}
		return result
	}
}
func AllPossibleCasesExecute(arr [][]string, str string) {
	if len(arr) == 0 {

	} else {
		for i := range arr[0] {
			AllPossibleCasesExecute(arr[1:], str+",,"+arr[0][i])
		}
	}
}

func AllPossibleCasesKnownSize(arr [][]string, result []string) []string {
	if len(arr) == 1 {
		return arr[0]
	} else {
		var allCasesOfRest = AllPossibleCasesKnownSize(arr[1:], result) // recur with the rest of array
		for i := 0; i < len(allCasesOfRest); i++ {
			for j := 0; j < len(arr[0]); j++ {
				result = append(result, arr[0][j]+",,"+allCasesOfRest[i])
			}
		}
	}
	return result
}

func (c *Cube) RunRules() {
	for i, r := range c.Rules {
		func(ruleInd int, rule Rule) {

			var detailedKeys [][]string
			prtCubeRef := prettyCubeRef(c, rule.Dimensions)
			casesSize := 1
			for j, ruleKey := range prtCubeRef {
				if ruleKey == "ANY" {
					dim := GetDimension(c.Dimensions[j])
					detailedKeys = append(detailedKeys, dim.GetLvlZero())
					casesSize *= len(dim.GetLvlZero())
				} else {
					detailedKeys = append(detailedKeys, []string{ruleKey})
				}
			}

			cases := AllPossibleCasesKnownSize(detailedKeys, []string{})
			for _, rKey := range cases {
				rKeyArr := strings.Split(rKey, ",,")
				ctx := Context{
					LeftSideKeys: nil,
					DetailedKeys: rKeyArr,
					Cube:         c,
				}
				val, err := rule.Evaluate(&ctx)
				valres := val
				switch val.(type) {
				case int:
					valres = float64(val.(int))
				}

				if err == nil {
					err = c.UpdateDetailedByRule(rKey, valres)
					if err != nil {
						fmt.Println("Error in updating detailed by rule: ", err)
					}
				}
				if err != nil {
					fmt.Println("Error in rule evaluate:", err)
				}
			}

		}(i, r)

	}

}

func (c *Cube) AllPossibleConsolidationsUpdate(addr string, arr [][]string, str string, diff float64, firstTime bool) {
	if len(arr) == 0 {
		if str != addr {
			c.ConsolidatedStorage.Lock()
			val, ok := c.ConsolidatedStorage.Storage[str]
			if ok {
				c.ConsolidatedStorage.Storage[str] = val.(float64) + diff
			} else {
				c.ConsolidatedStorage.Storage[str] = diff
			}
			c.ConsolidatedStorage.Unlock()
		}
	} else {
		for i := range arr[0] {
			if firstTime {
				c.AllPossibleConsolidationsUpdate(addr, arr[1:], arr[0][i], diff, false)
			} else {
				c.AllPossibleConsolidationsUpdate(addr, arr[1:], str+",,"+arr[0][i], diff, false)
			}

		}
	}
}
func GetCube(n string) *Cube {
	for i := 0; i < len(Cubes); i++ {
		if Cubes[i].Name == n {
			return Cubes[i]
		}
	}
	return nil
}

func AddSecurityCube(name string, dimension []string) {
	dimension = append(dimension, "$users")
	AddCube(InitCube("$"+name+"Security", dimension))
}

func AddCube(c *Cube) {
	if c.Name[0] != '$' {
		AddSecurityCube(c.Name, c.Dimensions)
	}
	Cubes = append(Cubes, c)

}

func InitCube(n string, ds []string) *Cube {
	return &Cube{
		Name:       n,
		Dimensions: ds,
		DetailedValues: Storage{
			Storage: make(map[string]interface{}),
		},
		ConsolidatedStorage: Storage{
			Storage: make(map[string]interface{}),
		},
		Rules: nil,
	}
}
func DeleteCube(n string) {
	for i := range Cubes {
		if Cubes[i].Name == n {
			Cubes[i] = Cubes[len(Cubes)-1]
			Cubes = Cubes[:len(Cubes)-1]
		}
	}
}
func (c *Cube) CheckDetailedFilledByRule(addr string) bool {
	return false
}
func (c *Cube) GetDimensionIndex(dimName string) int {
	for i := range c.Dimensions {
		if c.Dimensions[i] == dimName {
			return i
		}
	}
	return 0
}
func (c *Cube) CheckDetailedExist(addr string) (isExist bool, isConsolidated bool) {
	addrArr := strings.Split(addr, ",,")
	isConsolidated = false
	if len(addrArr) != len(c.Dimensions) {
		return false, false
	}
	for i, v := range c.Dimensions {
		d := GetDimension(v)
		if d == nil {
			return false, false
		}
		v := d.GetValue(addrArr[i])
		if v == nil {
			return false, false
		}
		for _, dv := range d.Hierarchy {
			if dv.Parent != nil && dv.Parent.Name == v.Name {
				isConsolidated = true
			}
		}
	}
	return true, isConsolidated
}

func equalRuleDetailed(a, b []string) bool {
	for i, v := range a {
		if v != b[i] && b[i] != "ANY" {
			return false
		}
	}
	return true
}
func (c *Cube) DetailedBelongsToRule(addr string) (bool, int) {
	for i, r := range c.Rules {
		prtCubeRef := prettyCubeRef(c, r.Dimensions)
		if equalRuleDetailed(strings.Split(addr, ",,"), prtCubeRef) {
			return true, i
		}
	}
	return false, 0
}

func (c *Cube) GetValueWithAccessCheck(addr, username string) (value interface{}, isConsalidated bool) {
	//cellSecurity := c.GetSecurityCube().GetDetailed(addr + ",," + username)
	//switch cellSecurity.(SecurityAccessType) {
	//case ReadWrite:
	//case Read:
	//	return c.GetValue(addr)
	//}

	return c.GetValue(addr)
	//return errors.New("access denied"), false
}

func (c *Cube) GetValue(addr string) (value interface{}, isConsolidated bool) {
	isExist, isConsolidated := c.CheckDetailedExist(addr)
	hasRule, ruleInd := c.DetailedBelongsToRule(addr)
	if isExist {
		if isConsolidated {
			return c.GetConsolidated(addr), true
		}
		if hasRule {
			rule := c.Rules[ruleInd]
			ctx := Context{
				LeftSideKeys: nil,
				DetailedKeys: strings.Split(addr, ",,"),
				Cube:         c,
			}
			val, err := rule.Evaluate(&ctx)
			if err != nil {
				val = "error" + err.Error()
			}
			return val, true
		}
		return c.GetDetailed(addr), false
	}
	return nil, false
}

func (c *Cube) UpdateRules(rulesRaw string) error {
	pwd, _ := os.Getwd()
	rulesPath := filepath.Join(pwd, "rules")
	cubeRulesPath := filepath.Join(rulesPath, c.Name+".sir")
	err := ioutil.WriteFile(cubeRulesPath, []byte(rulesRaw), 0644)
	if err != nil {
		return err
	}
	strings.Replace(rulesRaw, "\n", "", -1)
	rulesSlice := strings.Split(rulesRaw, ";")
	var rules []Rule
	for _, rule := range rulesSlice {
		if len(rule) > 4 {
			parsedRule, err := ParseRule(rule)
			if err != nil {
				return err
			}
			rules = append(rules, parsedRule)
		}
	}
	c.Rules = rules
	c.RunRules()
	return nil
}

func (c *Cube) GetConsolidated(addr string) interface{} {
	c.ConsolidatedStorage.Lock()
	val, ok := c.ConsolidatedStorage.Storage[addr]
	c.ConsolidatedStorage.Unlock()
	if !ok {
		return float64(0)
	}
	return val
}
func (c *Cube) GetSecurityCube() *Cube {
	for i := range Cubes {
		if Cubes[i].Name == "$"+c.Name+"Security" {
			return Cubes[i]
		}
	}
	return nil
}
func (c *Cube) GetDetailedWithAccessCheck(addr string, username string) interface{} {
	cellSecurity := c.GetSecurityCube().GetDetailed(addr + ",," + username)
	switch cellSecurity.(SecurityAccessType) {
	case ReadWrite:
	case Read:
		return c.GetDetailed(addr)
	}

	return errors.New("access denied")
}

func (c *Cube) GetDetailed(addr string) interface{} {
	c.DetailedValues.Lock()
	val, ok := c.DetailedValues.Storage[addr]
	c.DetailedValues.Unlock()
	if !ok {
		return float64(0)
	}
	return val
}

func (c *Cube) ConsolidateDetailed() {
	c.DetailedValues.Lock()
	for k, v := range c.DetailedValues.Storage {

		switch v.(type) {
		case float64:
			c.UpdateConsolidationsByDetailed(k, v.(float64))
			break
		}

	}
	c.DetailedValues.Unlock()
}

func (c *Cube) UpdateDetailedWithAccessCheck(addr string, username string, newVal interface{}) error {
	//cellSecurity := c.GetSecurityCube().GetDetailed(addr + ",," + username)
	//switch cellSecurity.(SecurityAccessType) {
	//case ReadWrite:
	//case Read:
	//	return c.UpdateDetailed(addr, newVal)
	//
	//}
	return c.UpdateDetailed(addr, newVal)
	//return errors.New("access denied")
}

func (c *Cube) UpdateDetailedByRule(addr string, newVal interface{}) error {
	//fmt.Print("UPDATE DETAILED: ")
	//fmt.Print(addr)
	//fmt.Print(" for cube ")
	//fmt.Println(c.Name)
	//fmt.Println("WITH VALUE: ", newVal)
	if isExist, isConsolidated := c.CheckDetailedExist(addr); isExist && !isConsolidated {

		c.DetailedValues.Lock()
		val, ok := c.DetailedValues.Storage[addr]
		c.DetailedValues.Unlock()

		if !ok {
			c.DetailedValues.Lock()
			c.DetailedValues.Storage[addr] = newVal
			switch newVal.(type) {
			case float64:
				c.UpdateConsolidationsByDetailed(addr, newVal.(float64))
			}

			c.DetailedValues.Unlock()
			return nil
		} else {

			var differenceBetweenNewAndPrevVal float64

			switch val.(type) {
			case string:
				switch newVal.(type) {
				case string:
					c.DetailedValues.Lock()
					c.DetailedValues.Storage[addr] = newVal
					c.DetailedValues.Unlock()
					break
				case float64:
					c.DetailedValues.Lock()
					c.DetailedValues.Storage[addr] = newVal
					c.DetailedValues.Unlock()
					differenceBetweenNewAndPrevVal = newVal.(float64)

					c.UpdateConsolidationsByDetailed(addr, differenceBetweenNewAndPrevVal)
					break
				}
				break
			case float64:
				switch newVal.(type) {
				case string:
					c.DetailedValues.Lock()
					c.DetailedValues.Storage[addr] = newVal
					c.DetailedValues.Unlock()
					differenceBetweenNewAndPrevVal = -val.(float64)

					c.UpdateConsolidationsByDetailed(addr, differenceBetweenNewAndPrevVal)
					break
				case float64:
					c.DetailedValues.Lock()
					c.DetailedValues.Storage[addr] = newVal
					c.DetailedValues.Unlock()
					differenceBetweenNewAndPrevVal = newVal.(float64) - val.(float64)
					c.UpdateConsolidationsByDetailed(addr, differenceBetweenNewAndPrevVal)
					break
				}
				break
			}

		}

	} else {
		return errors.New("Value doesn't exist or consolidated")
	}
	return nil
}
func (c *Cube) UpdateDetailed(addr string, newVal interface{}) error {
	//fmt.Print("UPDATE DETAILED: ")
	//fmt.Print(addr)
	//fmt.Print(" for cube ")
	//fmt.Println(c.Name)
	//fmt.Println("WITH VALUE: ", newVal)
	if isExist, isConsolidated := c.CheckDetailedExist(addr); isExist && !isConsolidated {

		c.DetailedValues.Lock()
		val, ok := c.DetailedValues.Storage[addr]
		c.DetailedValues.Unlock()

		if !ok {
			c.DetailedValues.Lock()
			c.DetailedValues.Storage[addr] = newVal
			switch newVal.(type) {
			case float64:
				c.UpdateConsolidationsByDetailed(addr, newVal.(float64))
			}

			c.DetailedValues.Unlock()
			return nil
		} else {

			var differenceBetweenNewAndPrevVal float64

			switch val.(type) {
			case string:
				switch newVal.(type) {
				case string:
					c.DetailedValues.Lock()
					c.DetailedValues.Storage[addr] = newVal
					c.DetailedValues.Unlock()
					break
				case float64:
					c.DetailedValues.Lock()
					c.DetailedValues.Storage[addr] = newVal
					c.DetailedValues.Unlock()
					differenceBetweenNewAndPrevVal = newVal.(float64)

					c.UpdateConsolidationsByDetailed(addr, differenceBetweenNewAndPrevVal)
					break
				}
				break
			case float64:
				switch newVal.(type) {
				case string:
					c.DetailedValues.Lock()
					c.DetailedValues.Storage[addr] = newVal
					c.DetailedValues.Unlock()
					differenceBetweenNewAndPrevVal = -val.(float64)

					c.UpdateConsolidationsByDetailed(addr, differenceBetweenNewAndPrevVal)
					break
				case float64:
					c.DetailedValues.Lock()
					c.DetailedValues.Storage[addr] = newVal
					c.DetailedValues.Unlock()
					differenceBetweenNewAndPrevVal = newVal.(float64) - val.(float64)
					c.UpdateConsolidationsByDetailed(addr, differenceBetweenNewAndPrevVal)
					break
				}
				break
			}

		}

		hasRule, _ := c.DetailedBelongsToRule(addr)
		if !hasRule {
			for _, cub := range Cubes {
				cub.RunRules()
			}

		}
	} else {
		return errors.New("Value doesn't exist or consolidated")
	}
	return nil
}

func (c *Cube) UpdateConsolidationsByDetailed(detailedAddr string, val float64) {
	cords := strings.Split(detailedAddr, ",,")
	coordsConsolidations := make([][]string, len(cords))
	for i, cord := range cords {
		d := GetDimension(c.Dimensions[i])
		if d != nil {
			dv := d.GetValue(cord)
			if dv != nil {
				cdv := dv.GetParentsList()
				cdv = append(cdv, cord)
				coordsConsolidations[i] = cdv
			}
		}
	}
	c.AllPossibleConsolidationsUpdate(detailedAddr, coordsConsolidations, "", val, true)
}
