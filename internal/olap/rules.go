package olap

import (
	"cloud.google.com/go/storage"
	"context"
	"github.com/alecthomas/participle"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func CheckRulesExist() bool {
	isGcloud := os.Getenv("GOOGLE_CLOUD_PROJECT")

	if isGcloud != "" {
		ctx := context.Background()
		client, _ := storage.NewClient(ctx)

		bkt := client.Bucket(isGcloud + ".appspot.com")

		it := bkt.Objects(ctx, nil)

		var hasBackups bool

		for {
			obj, err := it.Next()

			if err != nil {
				break
			}

			if strings.Contains(obj.Name, "rules") {
				hasBackups = true
			}
		}
		return hasBackups
	} else {
		pwd, _ := os.Getwd()
		_, err := ioutil.ReadDir(filepath.Join(pwd, "rules"))

		if err != nil {
			return false
		}
	}

	return true
}

func initRulesData() error {

	pwd, _ := os.Getwd()
	var backupPath string
	isGcloud := os.Getenv("GOOGLE_CLOUD_PROJECT")

	if isGcloud != "" {
		backupPath = "rules"
	} else {
		backupPath = filepath.Join(pwd, "rules")
		err := os.Mkdir(backupPath, 0777)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *Cube) GetRulesRaw() (string, error) {
	pwd, _ := os.Getwd()
	var rulesPath string
	isGcloud := os.Getenv("GOOGLE_CLOUD_PROJECT")

	if isGcloud != "" {
		rulesPath = "rules"
	} else {
		rulesPath = filepath.Join(pwd, "rules")
	}
	if !CheckRulesExist() {
		err := initRulesData()

		if err != nil {
			return "", err
		}
	}

	cubeRulesPath := filepath.Join(rulesPath, c.Name+".sir")
	rulesBytes, err := ReadFile(cubeRulesPath)
	if err != nil {
		err := WriteFile(cubeRulesPath, []byte(""))
		return "", err
	}
	return string(rulesBytes), nil
}

func ParseRule(str string) (Rule, error) {
	parser, err := participle.Build(&Rule{})
	if err != nil {
		return Rule{}, err
	}
	ast := &Rule{}
	err = parser.ParseString(str, ast)
	//spew.Dump(ast)
	if err != nil {
		return Rule{}, err
	}
	return *ast, nil
}
