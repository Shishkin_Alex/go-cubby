package olap

import (
	"cloud.google.com/go/storage"
	"context"
	"encoding/json"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func save(fileName string, v interface{}) error {
	js, err := json.Marshal(v)
	if err != nil {
		return err
	}
	d1 := js
	err = WriteFile(fileName, d1)
	if err != nil {
		return err
	}
	return nil
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func initBackupData() error {

	pwd, _ := os.Getwd()
	var backupPath string
	isGcloud := os.Getenv("GOOGLE_CLOUD_PROJECT")

	if isGcloud != "" {
		backupPath = "backup"
	} else {
		backupPath = filepath.Join(pwd, "backup")
		err := os.Mkdir(backupPath, 0777)
		if err != nil {
			return err
		}
	}

	// init dimensions backup file
	{

		// create admin user
		adminDV := DimensionValue{
			Name: "admin",
		}

		// init dimension with users and add admin user
		usersDim := Dimension{
			Name:      "$users",
			Hierarchy: []*DimensionValue{&adminDV},
		}

		var initDimensions []*Dimension

		initDimensions = append(initDimensions, &usersDim)

		// create password measure
		passwordDV := DimensionValue{
			Name: "password",
		}

		// create role measure
		roleDV := DimensionValue{
			Name: "role",
		}

		authDim := Dimension{
			Name:      "$authMeasure",
			Hierarchy: []*DimensionValue{&passwordDV, &roleDV},
		}

		initDimensions = append(initDimensions, &authDim)

		b, err := json.Marshal(initDimensions)

		if err != nil {
			return err
		}

		// create dimensions backup file
		if isGcloud == "" {
			CreateFileLocal(filepath.Join(backupPath, "dimensions.json"))
		}
		err = WriteFile(filepath.Join(backupPath, "dimensions.json"), b)
		if err != nil {
			return err
		}

	}

	// init cubes backup
	{
		// create auth cube
		authCube := InitCube("$auth", []string{"$users", "$authMeasure"})

		var initCubes []*Cube

		passHash, _ := HashPassword("WeAreSerious!")

		authCube.DetailedValues.Storage["admin,,password"] = passHash

		authCube.DetailedValues.Storage["admin,,role"] = AdminRole

		initCubes = append(initCubes, authCube)

		b, err := json.Marshal(initCubes)

		if err != nil {
			return err
		}

		// create cubes backup file
		if isGcloud == "" {
			CreateFileLocal(filepath.Join(backupPath, "cubes.json"))
		}
		err = WriteFile(filepath.Join(backupPath, "cubes.json"), b)
		if err != nil {
			return err
		}

	}

	return nil

}
func WriteFileGCloud(name string, data []byte) error {

	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return err
	}

	bkt := client.Bucket("cubby-backend.appspot.com")

	obj := bkt.Object(name)

	w := obj.NewWriter(ctx)

	defer w.Close()

	w.Write(data)

	return nil
}

func WriteFileLocal(path string, data []byte) error {
	err := ioutil.WriteFile(path, data, 0644)
	if err != nil {
		return err
	}
	return nil
}

func ReadFile(path string) ([]byte, error) {
	isGcloud := os.Getenv("GOOGLE_CLOUD_PROJECT")

	if isGcloud != "" {
		return ReadFileGCloud(path)
	} else {
		return ReadFileLocal(path)
	}
	return nil, nil
}

func ReadFileLocal(path string) ([]byte, error) {
	return ioutil.ReadFile(path)
}

func ReadFileGCloud(path string) ([]byte, error) {

	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	bkt := client.Bucket("cubby-backend.appspot.com")

	obj := bkt.Object(path)
	r, err := obj.NewReader(ctx)
	if err != nil {
		return nil, err
	}
	defer r.Close()

	data, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func WriteFile(path string, data []byte) error {
	isGcloud := os.Getenv("GOOGLE_CLOUD_PROJECT")

	if isGcloud != "" {
		return WriteFileGCloud(path, data)
	} else {
		return WriteFileLocal(path, data)
	}
	return nil
}

func CreateFileLocal(path string) error {
	isGcloud := os.Getenv("GOOGLE_CLOUD_PROJECT")

	if isGcloud != "" {
		emptyFile, err := os.Create(path)

		defer emptyFile.Close()

		return err
	}
	return nil
}

func CheckBackupExist() bool {
	isGcloud := os.Getenv("GOOGLE_CLOUD_PROJECT")

	if isGcloud != "" {
		ctx := context.Background()
		client, _ := storage.NewClient(ctx)

		bkt := client.Bucket(isGcloud + ".appspot.com")

		it := bkt.Objects(ctx, nil)

		var hasBackups bool

		for {
			obj, err := it.Next()

			if err != nil {
				break
			}

			if strings.Contains(obj.Name, "backup") {
				hasBackups = true
			}
		}
		return hasBackups
	} else {
		pwd, _ := os.Getwd()
		_, err := ioutil.ReadDir(filepath.Join(pwd, "backup"))

		if err != nil {
			return false
		}
	}

	return true
}

func LoadBackup() error {
	pwd, _ := os.Getwd()
	var backupPath string
	isGcloud := os.Getenv("GOOGLE_CLOUD_PROJECT")

	if isGcloud != "" {
		backupPath = "backup"
	} else {
		backupPath = filepath.Join(pwd, "backup")
	}

	// if backup folder doesn't exist generate init backup data
	if !CheckBackupExist() {
		err := initBackupData()

		if err != nil {
			return err
		}
	}

	backupDimensions, err := ReadFile(filepath.Join(backupPath, "dimensions.json"))
	if err != nil {
		fmt.Println(err)
		CreateFileLocal(filepath.Join(backupPath, "dimensions.json"))
	}
	err = json.Unmarshal(backupDimensions, &Dimensions)
	if err != nil {
		fmt.Println(err)
	}
	backupCubes, err := ReadFile(filepath.Join(backupPath, "cubes.json"))
	if err != nil {
		fmt.Println(err)
		CreateFileLocal(filepath.Join(backupPath, "cubes.json"))
	}
	err = json.Unmarshal(backupCubes, &Cubes)
	if err != nil {
		fmt.Println(err)
	}
	for i := range Cubes {
		Cubes[i].ConsolidatedStorage.Storage = map[string]interface{}{}

		var rulesPath string
		if isGcloud != "" {
			rulesPath = "rules"
		} else {
			pwd, _ := os.Getwd()
			rulesPath = filepath.Join(pwd, "rules")
		}
		cubeRulesPath := filepath.Join(rulesPath, Cubes[i].Name+".sir")
		rulesBytes, err := ReadFile(cubeRulesPath)
		if err != nil {
			fmt.Println(err, rulesPath)
			CreateFileLocal(cubeRulesPath)
		}
		rulesRaw := string(rulesBytes)
		strings.Replace(rulesRaw, "\n", "", -1)
		rulesSlice := strings.Split(rulesRaw, ";")
		var rules []Rule
		for _, rule := range rulesSlice {
			if len(rule) > 4 {
				parsedRule, err := ParseRule(rule)
				if err != nil {
					return err
				}
				rules = append(rules, parsedRule)
			}
		}
		Cubes[i].ConsolidateDetailed()
		Cubes[i].Rules = rules
		Cubes[i].RunRules()

	}
	return nil
}

func SaveBackup() error {
	pwd, _ := os.Getwd()
	var backupPath string
	isGcloud := os.Getenv("GOOGLE_CLOUD_PROJECT")

	if isGcloud != "" {
		backupPath = "backup"
	} else {
		backupPath = filepath.Join(pwd, "backup")

	}

	err := save(filepath.Join(backupPath, "cubes.json"), Cubes)
	if err != nil {
		return err
	}

	err = save(filepath.Join(backupPath, "dimensions.json"), Dimensions)
	if err != nil {
		return err
	}

	return nil
}
