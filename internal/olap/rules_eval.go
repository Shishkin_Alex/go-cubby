package olap

import (
	"errors"
	"fmt"
	"math"
	"strconv"
	"strings"
)

type Evaluable interface {
	Evaluate(ctx *Context) (interface{}, error)
}
type Context struct {
	LeftSideKeys []string
	DetailedKeys []string
	Cube         *Cube
}

func prettyCubeRef(c *Cube, cubeRefs []*CubeRef) []string {
	arr := make([]string, len(c.Dimensions))
	for _, cR := range cubeRefs {
		var dimInd int
		if cR.Dimension != "" {
			for i, dName := range c.Dimensions {
				if dName == cR.Dimension {
					dimInd = i
				}
			}
		} else {
			for i, dName := range c.Dimensions {
				d := GetDimension(dName)
				if d.GetValue(cR.Name) != nil {
					dimInd = i
				}
			}
		}
		arr[dimInd] = cR.Name
	}
	for i, arrV := range arr {
		if arrV == "" {
			arr[i] = "ANY"
		}
	}
	return arr
}
func (v *Value) Evaluate(ctx *Context) (interface{}, error) {
	switch {
	case v.Number != nil:
		return *v.Number, nil
	case v.Int != nil:
		return float64(*v.Int), nil
	case v.String != nil:
		return *v.String, nil
	case v.Dimension != nil:
		for i, d := range ctx.Cube.Dimensions {
			if d == *v.Dimension {
				if ctx.LeftSideKeys[i] != "ANY" {
					return ctx.LeftSideKeys[i], nil
				} else {
					return ctx.DetailedKeys[i], nil
				}
			}
		}
		return "", errors.New("wrong dimension name: " + *v.Dimension)
	case v.CubeRef != nil:
		prtKeys := prettyCubeRef(ctx.Cube, v.CubeRef)
		for i := range prtKeys {
			if prtKeys[i] == "ANY" {
				prtKeys[i] = ctx.DetailedKeys[i]
			}
		}
		val, _ := GetCube(ctx.Cube.Name).GetValue(strings.Join(prtKeys, ",,"))
		return val, nil
	case v.DBRef != nil:
		var params []string
		for _, b := range v.DBRef.Args {
			str, err := b.Evaluate(ctx)
			if err != nil {
				return nil, err
			}
			params = append(params, fmt.Sprintf("%v", str))
		}
		// params[0] - cube name
		// params[1:] - address
		val, _ := GetCube(params[0]).GetValue(strings.Join(params[1:], ",,"))
		return val, nil
	case v.Subexpression != nil:
		return v.Subexpression.Evaluate(ctx)
	}
	return nil, errors.New("unsupported value type")
}

func (b *Body) Evaluate(ctx *Context) (interface{}, error) {
	switch {
	case b.Value != nil:
		return b.Value.Evaluate(ctx)
	case b.Command != nil:
		return b.Command.Evaluate(ctx)
	}
	return nil, errors.New("something went wrong in body")
}

func (c *Commands) Evaluate(ctx *Context) (interface{}, error) {
	switch {
	case c.Attrs != nil:
		return c.Attrs.Evaluate(ctx)
	case c.If != nil:
		return c.If.Evaluate(ctx)
	case c.NumToStr != nil:
		return c.NumToStr.Evaluate(ctx)
	}
	return nil, errors.New("something went wrong in commands")
}
func compareBody(b bool, left *Addition, right *Addition, ctx *Context) (interface{}, error) {
	if b {
		return left.Evaluate(ctx)
	} else {
		return right.Evaluate(ctx)
	}
}
func (a *Attrs) Evaluate(ctx *Context) (interface{}, error) {
	dim, err := a.Args[0].Evaluate(ctx)
	if err != nil {
		return nil, err
	}
	dv, err := a.Args[1].Evaluate(ctx)
	if err != nil {
		return nil, err
	}
	key, err := a.Args[2].Evaluate(ctx)
	if err != nil {
		return nil, err
	}
	return GetDimension(dim.(string)).GetValue(dv.(string)).GetAttribute(key.(string))
}
func (i *If) Evaluate(ctx *Context) (interface{}, error) {
	lhs, err := i.ConditionLeft.Evaluate(ctx)
	if err != nil {
		return nil, err
	}
	rhs, err := i.ConditionRight.Evaluate(ctx)
	if err != nil {
		return nil, err
	}
	switch *i.ConditionOp {
	case "==":
		switch lhs.(type) {
		case string:
			return compareBody(lhs.(string) == rhs.(string), i.ValLeft, i.ValRight, ctx)
		case float64:
			return compareBody(lhs.(float64) == rhs.(float64), i.ValLeft, i.ValRight, ctx)
		}

	case "!=":
		return compareBody(lhs.(float64) != rhs.(float64), i.ValLeft, i.ValRight, ctx)
	case "<":
		return compareBody(lhs.(float64) < rhs.(float64), i.ValLeft, i.ValRight, ctx)
	case ">":
		return compareBody(lhs.(float64) > rhs.(float64), i.ValLeft, i.ValRight, ctx)
	case "<=":
		return compareBody(lhs.(float64) <= rhs.(float64), i.ValLeft, i.ValRight, ctx)
	case ">=":
		return compareBody(lhs.(float64) >= rhs.(float64), i.ValLeft, i.ValRight, ctx)
	}

	return nil, errors.New("something went wrong in if")
}
func (n *NumToStr) Evaluate(ctx *Context) (interface{}, error) {
	v, err := n.Args[0].Evaluate(ctx)
	if err != nil {
		return nil, err
	}
	return strconv.Itoa(int(v.(float64))), nil
}
func (r *Rule) Evaluate(ctx *Context) (interface{}, error) {
	ctx.LeftSideKeys = prettyCubeRef(ctx.Cube, r.Dimensions)
	return r.RuleBody.Evaluate(ctx)
}
func (t *TypeBody) Evaluate(ctx *Context) (interface{}, error) {
	return t.Body.Evaluate(ctx)
	//return nil, errors.New("how u got here")
}
func (a *Addition) Evaluate(ctx *Context) (interface{}, error) {
	if a.Next != nil {
		lhsNumber, err := a.Multiplication.Evaluate(ctx)
		rhsNumber, err := a.Next.Evaluate(ctx)
		if err != nil {
			return nil, err
		}
		switch a.Op {
		case "+":
			return lhsNumber.(float64) + rhsNumber.(float64), nil
		case "-":
			return lhsNumber.(float64) - rhsNumber.(float64), nil
		}
	} else {
		return a.Multiplication.Evaluate(ctx)
	}

	return nil, errors.New("how u got here addition")
}
func (m *Multiplication) Evaluate(ctx *Context) (interface{}, error) {
	if m.Next != nil {
		lhsNumber, err := m.Body.Evaluate(ctx)
		if err != nil {
			return nil, err
		}
		rhsNumber, err := m.Next.Evaluate(ctx)

		if err != nil {
			return nil, err
		}
		switch m.Op {
		case "*":
			if ctx.Cube.Name == "salaryMain" && lhsNumber.(float64) != 0 && rhsNumber.(float64) != 0 {
				fmt.Println("Multiply ", lhsNumber, "*", rhsNumber)
			}
			var lhsVal, rhsVal float64
			switch lhsNumber.(type) {
			case int:
				lhsVal = float64(lhsNumber.(int))
				break
			case float64:
				lhsVal = lhsNumber.(float64)
				break
			}
			switch rhsNumber.(type) {
			case int:
				rhsVal = float64(rhsNumber.(int))
				break
			case float64:
				rhsVal = rhsNumber.(float64)
				break
			}
			return lhsVal * rhsVal, nil
		case "/":
			var lhsVal, rhsVal float64
			switch lhsNumber.(type) {
			case int:
				lhsVal = float64(lhsNumber.(int))
				break
			case float64:
				lhsVal = lhsNumber.(float64)
				break
			}
			switch rhsNumber.(type) {
			case int:
				rhsVal = float64(rhsNumber.(int))
				break
			case float64:
				rhsVal = rhsNumber.(float64)
				break
			}
			if rhsVal == 0 {
				return 0, nil
			}
			return lhsVal / rhsVal, nil

		case "^":
			pow := math.Pow(lhsNumber.(float64), rhsNumber.(float64))
			return pow, nil
		}
	} else {
		return m.Body.Evaluate(ctx)
	}

	return nil, errors.New("how u got here multip")
}
