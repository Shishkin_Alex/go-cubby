package olap

func GetUserPasswordByName(name string) string {
	authCube := GetCube("$auth")

	password, _ := authCube.GetValue(name + ",,password")

	return password.(string)
}

func GetUserRoleByName(name string) UserRoleType {
	authCube := GetCube("$auth")

	password, _ := authCube.GetValue(name + ",,role")

	switch password.(type) {
	case int:
		return UserRoleType(password.(int))
	case float64:
		return UserRoleType(password.(float64))
	}
	return UserRoleType(0)
}

func GetUserRoleByNameAsString(name string) string {
	role := GetUserRoleByName(name)

	switch role {
	case AdminRole:
		return "admin"
	default:
		return "guest"
	}
}
