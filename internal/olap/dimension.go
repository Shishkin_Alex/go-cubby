package olap

import (
	"errors"
)

type Dimension struct {
	Name       string
	Hierarchy  []*DimensionValue
	Subsets    []*Subset
	Attributes []Attribute
}

type FlatHierarchy []*DimensionValue

var Dimensions []*Dimension

type DimensionValue struct {
	Level      int
	Name       string
	Parent     *DimensionValue
	Children   []*DimensionValue
	Attributes map[string]interface{}
}

type Subset struct {
	Name      string
	Hierarchy []*DimensionValue
}

type Attribute struct {
	Name string
	Type string
}

func GetDimension(n string) *Dimension {
	for i := 0; i < len(Dimensions); i++ {
		if Dimensions[i].Name == n {
			return Dimensions[i]
		}
	}
	return nil
}

func AddDimension(d *Dimension) {
	Dimensions = append(Dimensions, d)
}
func DeleteDimension(n string) {
	for i := range Dimensions {
		if Dimensions[i].Name == n {
			Dimensions[i] = Dimensions[len(Dimensions)-1]
			Dimensions = Dimensions[:len(Dimensions)-1]
		}
	}
}

func (d *Dimension) GetValue(n string) *DimensionValue {
	for i := 0; i < len(d.Hierarchy); i++ {
		if d.Hierarchy[i].Name == n {
			return d.Hierarchy[i]
		}
	}
	return nil
}
func (d *Dimension) AddAttribute(n string, t string) {
	d.Attributes = append(d.Attributes, Attribute{
		Name: n,
		Type: t,
	})
}
func (fh *FlatHierarchy) AppendChild(dv *DimensionValue) {
	*fh = append(*fh, dv)
	if len(dv.Children) > 0 {
		for i := range dv.Children {
			fh.AppendChild(dv.Children[i])
		}
	}
}
func (d *Dimension) FlatHierarchy() *FlatHierarchy {
	res := FlatHierarchy{}
	for i := range d.Hierarchy {
		res.AppendChild(d.Hierarchy[i])
	}
	return &res

}
func (d *Dimension) GetSubset(name string) *Subset {
	for i := range d.Subsets {
		if d.Subsets[i].Name == name {
			return d.Subsets[i]
		}
	}
	return nil
}
func (d *Dimension) AddSubset(subset *Subset) {
	d.Subsets = append(d.Subsets, subset)
}
func (d *Dimension) DeleteSubset(name string) {
	for i := range d.Subsets {
		if d.Subsets[i].Name == name {
			d.Subsets[i] = d.Subsets[len(d.Subsets)-1]
			d.Subsets = d.Subsets[:len(d.Subsets)-1]
		}
	}
}
func (d *Dimension) GetHierarchy(level int) []*DimensionValue {
	var dvs []*DimensionValue
	for _, dv := range d.Hierarchy {
		if dv.Level == level {
			dvs = append(dvs, dv)
		}
	}
	return dvs
}
func (dv *DimensionValue) SetAttribute(key string, val interface{}) {
	if dv.Attributes == nil {
		dv.Attributes = make(map[string]interface{})
	}
	dv.Attributes[key] = val
}
func (dv *DimensionValue) GetAttribute(key string) (interface{}, error) {
	val, ok := dv.Attributes[key]
	if !ok {
		return nil, errors.New("attribute doesn't exist")
	}
	return val, nil
}

func (dv *DimensionValue) GetParentsList() []string {
	var result []string
	if dv != nil {
		tempParent := dv.Parent
		for tempParent != nil {
			result = append(result, tempParent.Name)
			tempParent = tempParent.Parent
		}
	}

	return result
}
func (d *Dimension) HasChild(dv *DimensionValue) bool {

	for _, dval := range d.Hierarchy {
		if dval.Parent != nil && dval.Parent.Name == dv.Name {
			return false
		}
	}
	return true
}
func (d *Dimension) GetLvlZero() []string {
	var res []string
	for _, dv := range d.Hierarchy {
		if d.HasChild(dv) {
			res = append(res, dv.Name)
		}
	}
	return res
}
