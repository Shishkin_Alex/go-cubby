package integrator

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
)

func GetProcessesList() ([]string, error) {
	list := make([]string, 0)
	tiPath, ok := os.LookupEnv("TI_PATH")
	if !ok {
		return nil, errors.New("TI_PATH env var doesnt exist")
	}
	files, err := ioutil.ReadDir(tiPath)
	if err != nil {
		return nil, errors.New("error in reading dir")
	}
	for _, f := range files {
		if f.IsDir() {
			dirFiles, err := ioutil.ReadDir(tiPath + "/" + f.Name())
			if err != nil {
				return nil, errors.New("error in reading dir")
			}
			for _, dF := range dirFiles {
				if dF.Name() == "main.go" {
					list = append(list, f.Name())
				}
			}
		}

	}
	return list, nil
}
func AddProcess(n string) error {
	tiPath, ok := os.LookupEnv("TI_PATH")
	if !ok {
		return errors.New("TI_PATH env var doesnt exist")
	}
	path := filepath.Join(tiPath, n)
	err := os.MkdirAll(path, os.ModePerm)
	if err != nil {
		return err
	}
	presetMain := []byte("package main\n\nfunc main(){\n\n}")
	mainPath := filepath.Join(path, "main.go")
	err = ioutil.WriteFile(mainPath, presetMain, 0644)
	if err != nil {
		return err
	}
	return nil
}

func GetProcessFileList(n string) ([]string, error) {
	tiPath, ok := os.LookupEnv("TI_PATH")
	if !ok {
		return nil, errors.New("TI_PATH env var doesnt exist")
	}
	path := filepath.Join(tiPath, n)
	dirFiles, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, errors.New("error in reading dir")
	}
	var list []string
	for _, dF := range dirFiles {
		list = append(list, dF.Name())
	}
	return list, nil
}

func GetProcessFileBody(process string, name string) (string, error) {
	tiPath, ok := os.LookupEnv("TI_PATH")
	if !ok {
		return "", errors.New("TI_PATH env var doesnt exist")
	}
	dirPath := filepath.Join(tiPath, process)
	filePath := filepath.Join(dirPath, name)
	fmt.Println(filePath)
	b, err := ioutil.ReadFile(filePath)
	if err != nil {
		return "", errors.New("no such file")
	}
	return string(b), nil
}

func UpdateProcessFile(process string, fileName string, text string) error {
	tiPath, ok := os.LookupEnv("TI_PATH")
	if !ok {
		return errors.New("TI_PATH env var doesnt exist")
	}
	dirPath := filepath.Join(tiPath, process)
	filePath := filepath.Join(dirPath, fileName)
	fmt.Println(filePath, text)
	err := ioutil.WriteFile(filePath, []byte(text), 0644)
	if err != nil {
		return err
	}
	return nil
}
func AddProcessFile(process string, fileName string) error {
	tiPath, ok := os.LookupEnv("TI_PATH")
	if !ok {
		return errors.New("TI_PATH env var doesnt exist")
	}
	path := filepath.Join(tiPath, process)
	presetMain := []byte("package main\n")
	mainPath := filepath.Join(path, fileName)
	err := ioutil.WriteFile(mainPath, presetMain, 0644)
	if err != nil {
		return err
	}
	return nil
}
func DeleteProcessFile(process string, fileName string) error {
	tiPath, ok := os.LookupEnv("TI_PATH")
	if !ok {
		return errors.New("TI_PATH env var doesnt exist")
	}
	path := filepath.Join(tiPath, process)
	mainPath := filepath.Join(path, fileName)
	err := os.Remove(mainPath)
	if err != nil {
		return err
	}
	return nil
}
func RunProcess(process string) error {
	tiPath, ok := os.LookupEnv("TI_PATH")
	if !ok {
		return errors.New("TI_PATH env var doesnt exist")
	}
	dirPath := filepath.Join(tiPath, process)
	exec_cmd("go", "run", dirPath, ".")

	return nil
}
func exec_cmd(cmd string, args ...string) {
	command := exec.Command(cmd, args...)
	var out bytes.Buffer
	var stderr bytes.Buffer
	command.Stdout = &out
	command.Stderr = &stderr
	err := command.Run()
	if err != nil {
		fmt.Println(fmt.Sprint(err) + ": " + stderr.String())
	}

	fmt.Println(out.String())
}

func RunProcessWithParams(process string, params map[int]map[int]string) error {
	tiPath, ok := os.LookupEnv("TI_PATH")
	if !ok {
		return errors.New("TI_PATH env var doesnt exist")
	}
	presetMain := "package main\nfunc InitDevParams() map[int]map[int]string {\ndevParams := map[int]map[int]string{}\n"
	for row := range params {
		presetMain += "devParams[" + strconv.Itoa(row) + "]= map[int]string{}\n"
		for col, val := range params[row] {
			presetMain += "devParams[" + strconv.Itoa(row) + "][" + strconv.Itoa(col) + "] = \"" + val + "\"\n"
		}
	}
	presetMain += "return devParams\n}"
	presetMainByte := []byte(presetMain)
	dirPath := filepath.Join(tiPath, process)
	mainPath := filepath.Join(dirPath, "subnmParams.go")
	err := ioutil.WriteFile(mainPath, presetMainByte, 0644)
	if err != nil {
		return err
	}
	go exec_cmd("go", "run", dirPath, ".")

	return nil
}
