package forms

import (
	"cloud.google.com/go/storage"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

var Forms []*Form

type Cell struct {
	Styles string
	Value  string
}

type Form struct {
	Cells      [][]Cell
	Name       string
	Properties FormProperties
}

type FormProperties struct {
}

type UserForm struct {
	SUBNM map[int]map[int]SUBNMContext
	Form
	Meta map[int]map[int]MetaInfo
}

type MetaInfo struct {
	Editable bool
	Type     string
}

func save(fileName string, v interface{}) error {
	js, err := json.Marshal(v)
	if err != nil {
		return err
	}
	d1 := js
	err = WriteFile(fileName, d1)
	if err != nil {
		return err
	}
	return nil
}
func WriteFileGCloud(name string, data []byte) error {

	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return err
	}

	bkt := client.Bucket("cubby-backend.appspot.com")

	obj := bkt.Object(name)

	w := obj.NewWriter(ctx)

	defer w.Close()

	w.Write(data)

	return nil
}

func WriteFileLocal(path string, data []byte) error {
	err := ioutil.WriteFile(path, data, 0644)
	if err != nil {
		return err
	}
	return nil
}

func ReadFile(path string) ([]byte, error) {
	isGcloud := os.Getenv("GOOGLE_CLOUD_PROJECT")

	if isGcloud != "" {
		return ReadFileGCloud(path)
	} else {
		return ReadFileLocal(path)
	}
	return nil, nil
}

func ReadFileLocal(path string) ([]byte, error) {
	return ioutil.ReadFile(path)
}

func ReadFileGCloud(path string) ([]byte, error) {

	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	bkt := client.Bucket("cubby-backend.appspot.com")

	obj := bkt.Object(path)
	r, err := obj.NewReader(ctx)
	if err != nil {
		return nil, err
	}
	defer r.Close()

	data, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func WriteFile(path string, data []byte) error {
	isGcloud := os.Getenv("GOOGLE_CLOUD_PROJECT")

	if isGcloud != "" {
		return WriteFileGCloud(path, data)
	} else {
		return WriteFileLocal(path, data)
	}
	return nil
}

func SaveBackup() error {
	pwd, _ := os.Getwd()
	var backupPath string
	isGcloud := os.Getenv("GOOGLE_CLOUD_PROJECT")

	if isGcloud != "" {
		backupPath = "backup"
	} else {
		backupPath = filepath.Join(pwd, "backup")

	}
	err := save(filepath.Join(backupPath, "forms.json"), Forms)
	if err != nil {
		fmt.Println(err)
		return errors.New("failed to save file")
	}
	return nil
}
func CreateFileLocal(path string) error {
	isGcloud := os.Getenv("GOOGLE_CLOUD_PROJECT")

	if isGcloud != "" {
		emptyFile, err := os.Create(path)

		defer emptyFile.Close()

		return err
	}
	return nil
}

func LoadBackup() error {
	pwd, _ := os.Getwd()
	var backupPath string
	isGcloud := os.Getenv("GOOGLE_CLOUD_PROJECT")

	if isGcloud != "" {
		backupPath = "backup"
	} else {
		backupPath = filepath.Join(pwd, "backup")
	}
	backupForms, err := ReadFile(filepath.Join(backupPath, "forms.json"))
	if err != nil {
		CreateFileLocal(filepath.Join(backupPath, "forms.json"))
	}
	var savedForms []Form
	err = json.Unmarshal(backupForms, &savedForms)
	if err != nil {
		return err
	}
	for _, f := range savedForms {
		newF := f
		Forms = append(Forms, &newF)
	}
	return nil
}
func AddForm(f Form) {
	Forms = append(Forms, &f)
}
func DeleteForm(n string) {
	for i, f := range Forms {
		if f.Name == n {
			Forms[i] = Forms[len(Forms)-1]
			Forms = Forms[:len(Forms)-1]
		}
	}
}
func GetForm(n string) *Form {
	for _, f := range Forms {
		if f.Name == n {
			return f
		}
	}
	return nil
}
func GetFormParsed(n string, userInput map[int]map[int]interface{}, username string) UserForm {
	form := *GetForm(n)
	newForm := Form{
		Cells: make([][]Cell, len(form.Cells)),
		Name:  n,
	}
	formRes := UserForm{
		SUBNM: map[int]map[int]SUBNMContext{},
		Form:  newForm,
		Meta:  map[int]map[int]MetaInfo{},
	}
	for i := range newForm.Cells {
		newForm.Cells[i] = make([]Cell, len(form.Cells[i]))
		for j := range newForm.Cells[i] {
			fC := form.Cells[i][j]
			if len(fC.Value) > 0 && string([]rune(fC.Value)[0]) == "=" {
				ast, err := ParseFormula(fC.Value)
				if err != nil {
					fmt.Println("Error in gformula parsing: ", err)
				}
				ctx := Context{
					Row:            i,
					Col:            j,
					Form:           form,
					Result:         nil,
					UserInput:      userInput,
					SUBNM:          SUBNMContext{},
					isConsolidated: false,
					Username:       username,
				}
				res, err := ast.Evaluate(&ctx)
				if err != nil {
					newForm.Cells[i][j] = Cell{
						Styles: fC.Styles,
						Value:  fmt.Sprintf("%v", err.Error()),
					}
				} else {
					var resStr string
					resStr = fmt.Sprintf("%v", res)
					switch res.(type) {
					case float64:
						resStr = strconv.FormatFloat(res.(float64), 'f', -1, 64)
					}
					newForm.Cells[i][j] = Cell{
						Styles: fC.Styles,
						Value:  resStr,
					}
				}

				if strings.Contains(fC.Value, "SUBNM") {
					if len(formRes.SUBNM[i]) == 0 {
						formRes.SUBNM[i] = map[int]SUBNMContext{}
					}
					formRes.SUBNM[i][j] = ctx.SUBNM
					if len(formRes.Meta[i]) == 0 {
						formRes.Meta[i] = map[int]MetaInfo{}
					}
					if err != nil {
						formRes.Meta[i][j] = MetaInfo{Editable: false}
					} else {
						formRes.Meta[i][j] = MetaInfo{Editable: true}
					}
				} else if string([]rune(fC.Value)[0:5]) == "=DBRW" {
					if len(formRes.Meta[i]) == 0 {
						formRes.Meta[i] = make(map[int]MetaInfo)
					}
					if err != nil {
						formRes.Meta[i][j] = MetaInfo{Editable: false}
					} else {
						formRes.Meta[i][j] = MetaInfo{Editable: !ctx.isConsolidated}
					}
				} else {
					if len(formRes.Meta[i]) == 0 {
						formRes.Meta[i] = map[int]MetaInfo{}
					}
					formRes.Meta[i][j] = MetaInfo{Editable: false}
				}
			} else {
				newForm.Cells[i][j] = Cell{
					Styles: fC.Styles,
					Value:  form.Cells[i][j].Value,
				}
				if len(formRes.Meta[i]) == 0 {
					formRes.Meta[i] = map[int]MetaInfo{}
				}
				formRes.Meta[i][j] = MetaInfo{Editable: false}
			}
		}
	}
	return formRes
}
func UpdateForm(f Form) *Form {
	oldForm := GetForm(f.Name)
	oldForm.Cells = f.Cells
	return oldForm
}
