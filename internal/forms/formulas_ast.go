package forms

type Formula struct {
	Body  *Body  `"=" @@`
	Value *Value "|@@"
}
type Body struct {
	Index int
	Body  *Addition `@@`
}
type Addition struct {
	Multiplication *Multiplication `@@`
	Op             string          `[ @( "-" | "+" )`
	Next           *Addition       `  @@ ]`
}

type Multiplication struct {
	Body *InnerBody      `@@`
	Op   string          `[ @( "/" | "*" | "^" )`
	Next *Multiplication `  @@ ]`
}

type InnerBody struct {
	Index    int
	Function *Formulas `@@`
	Value    *Value    `| @@`
}

type Formulas struct {
	Index  int
	Dbrw   *DBRW   "@@"
	Subnm  *SUBNM  "| @@"
	Attr   *Attr   "| @@"
	If     *If     "| @@"
	String *String "|@@"
}
type If struct {
	Index          int
	ConditionLeft  *Body   `"IF""(" @@ `
	ConditionOp    *string `@(">" | "<" | ">""=" | "<""=" | "=""=" | "!""=")*`
	ConditionRight *Body   `@@ ","`
	ValLeft        *Body   `@@ ","`
	ValRight       *Body   `@@ ")"`
}
type String struct {
	Index int
	Args  *Body `"String""("  @@ ")"`
}

type SUBNM struct {
	Index int
	Args  []*Body `"SUBNM""(" ( @@ ( "," @@ )* )? ")"`
}
type Attr struct {
	Index int
	Args  []*Body `"ATTR""(" ( @@ ( "," @@ )* )? ")"`
}
type DBRW struct {
	Index int
	Args  []*Body `"DBRW""(" ( @@ ( "," @@ )* )? ")"`
}

type Value struct {
	Index  int
	Ref    *string  `@Ident`
	String *string  `| @String`
	Int    *int     `| @Int`
	Number *float64 `| @Float`
}
