package forms

import (
	"bitbucket.org/Shishkin_Alex/go-cubby/internal/olap"
	"errors"
	"fmt"
	"math"
	"reflect"
	"regexp"
	"strconv"
	"strings"
)

type SUBNMContext struct {
	Attr            string
	DimensionValues []olap.DimensionValue
	IsDBRW          bool
}
type Context struct {
	Row            int
	Col            int
	Id             string
	Form           Form
	SUBNM          SUBNMContext
	UserInput      map[int]map[int]interface{}
	Result         [][]interface{}
	isConsolidated bool
	Perms          map[string]map[string]string
	Username       string
}

func interfaceToFloat(i interface{}) float64 {
	switch i.(type) {
	case string:
		if s, err := strconv.ParseFloat(i.(string), 64); err == nil {
			return s
		}
	case float64:
		return i.(float64)
	}
	return float64(0)
}

func (a *Addition) Evaluate(ctx *Context) (interface{}, error) {
	if a.Next != nil {
		lhsInterface, err := a.Multiplication.Evaluate(ctx)
		rhsInterface, err := a.Next.Evaluate(ctx)
		if err != nil {
			return nil, err
		}

		switch a.Op {
		case "+":
			var res interface{}
			switch lhsInterface.(type) {
			case string:
				res = lhsInterface.(string) + fmt.Sprintf("%v", rhsInterface)
				break
			case float64:
				switch rhsInterface.(type) {
				case nil:
					lhsNumber := interfaceToFloat(lhsInterface)
					res = lhsNumber
					break
				case string:
					res = fmt.Sprintf("%v", lhsInterface) + fmt.Sprintf("%v", rhsInterface)
					break
				case float64:
					lhsNumber := interfaceToFloat(lhsInterface)
					rhsNumber := interfaceToFloat(rhsInterface)
					res = lhsNumber + rhsNumber
					break
				}

				break
			}
			return res, nil
		case "-":

			lhsNumber := interfaceToFloat(lhsInterface)
			rhsNumber := interfaceToFloat(rhsInterface)
			return lhsNumber - rhsNumber, nil
		}
	} else {
		return a.Multiplication.Evaluate(ctx)
	}

	return nil, errors.New("how u got here addition")
}
func compareFormulaBody(b bool, left *Body, right *Body, ctx *Context) (interface{}, error) {
	if b {
		return left.Evaluate(ctx)
	} else {
		return right.Evaluate(ctx)
	}
}
func (i *If) Evaluate(ctx *Context) (interface{}, error) {
	lhs, err := i.ConditionLeft.Evaluate(ctx)
	if err != nil {
		return nil, err
	}
	rhs, err := i.ConditionRight.Evaluate(ctx)
	if err != nil {
		return nil, err
	}
	fmt.Println("COMPARE VALUES: ", fmt.Sprintf("%v", lhs), fmt.Sprintf("%v", *i.ConditionOp), fmt.Sprintf("%v", rhs))
	switch *i.ConditionOp {
	case "==":
		return compareFormulaBody(lhs.(float64) == rhs.(float64), i.ValLeft, i.ValRight, ctx)
	case "!=":
		return compareFormulaBody(lhs.(float64) != rhs.(float64), i.ValLeft, i.ValRight, ctx)
	case "<":
		return compareFormulaBody(lhs.(float64) < rhs.(float64), i.ValLeft, i.ValRight, ctx)
	case ">":
		return compareFormulaBody(lhs.(float64) > rhs.(float64), i.ValLeft, i.ValRight, ctx)
	case "<=":
		return compareFormulaBody(lhs.(float64) <= rhs.(float64), i.ValLeft, i.ValRight, ctx)
	case ">=":
		return compareFormulaBody(lhs.(float64) >= rhs.(float64), i.ValLeft, i.ValRight, ctx)
	}
	return nil, errors.New("something went wrong in if")
}
func (m *Multiplication) Evaluate(ctx *Context) (interface{}, error) {
	if m.Next != nil {
		lhsNumber, err := m.Body.Evaluate(ctx)
		if err != nil {
			return nil, err
		}
		switch lhsNumber.(type) {
		case string:
			lhsNumber = float64(0)
		}
		rhsNumber, err := m.Next.Evaluate(ctx)
		switch rhsNumber.(type) {
		case string:
			lhsNumber = float64(0)
		}
		if err != nil {
			return nil, err
		}
		switch m.Op {
		case "*":
			return lhsNumber.(float64) * rhsNumber.(float64), nil
		case "/":
			return lhsNumber.(float64) / rhsNumber.(float64), nil
		case "^":
			pow := math.Pow(lhsNumber.(float64), rhsNumber.(float64))
			return pow, nil
		}
	} else {
		return m.Body.Evaluate(ctx)
	}
	return nil, errors.New("how u got here multip")
}
func (b *InnerBody) Evaluate(ctx *Context) (interface{}, error) {
	switch {
	case b.Value != nil:
		return b.Value.Evaluate(ctx)
	case b.Function != nil:
		return b.Function.Evaluate(ctx)
	}
	return nil, errors.New("body is empty")
}
func letterToColumn(letter string) float64 {
	var column float64
	var length = len(letter)
	for i := 0; i < length; i++ {
		column += float64(charCodeAt(letter, i)-64) * math.Pow(26, float64(length-i-1))
	}
	return column
}
func charCodeAt(s string, n int) rune {
	i := 0
	for _, r := range s {
		if i == n {
			return r
		}
		i++
	}
	return 0
}
func contains(slice []string) map[string]struct{} {
	set := make(map[string]struct{}, len(slice))
	for _, s := range slice {
		set[s] = struct{}{}
	}

	return set
}
func HasPermissionsDetailed(cubeName string, dvKeys []string, id string) string {
	// permCube := olap.GetCube("][" + cubeName)
	// if permCube == nil {
	// 	return "none"
	// }
	// permDv := append(dvKeys, id)
	// // detailed, ok := permCube.Values.Load(strings.Join(permDv, ",,"))
	// if !ok {
	// 	return "none"
	// }
	// val := detailed.(DetailedValue).Value
	// if val == 2 {
	// 	return "rw"
	// }
	// if val == 1 {
	// 	return "r"
	// }
	return "none"
}
func getFormValue(ctx *Context, address string) (interface{}, error) {
	r, _ := regexp.Compile("[a-zA-Z$]+")
	rInt, _ := regexp.Compile("[0-9]+")
	colLetter := r.FindString(address)
	rowInt, _ := strconv.Atoi(rInt.FindString(address))
	colIndex := int(letterToColumn(colLetter))
	if len(ctx.Form.Cells) < rowInt-1 {
		return nil, errors.New("unresolved row " + strconv.Itoa(rowInt))
	}
	if len(ctx.Form.Cells[rowInt-1]) < colIndex-1 {
		return nil, errors.New("unresolved col " + colLetter)
	}
	valText := ctx.Form.Cells[rowInt-1][colIndex-1].Value
	if len(valText) > 0 && string([]rune(valText)[0]) == "=" {
		refParsed, err := ParseFormula(valText)
		if err != nil {
			return nil, errors.New("can't parse ref")
		}
		newCtx := *ctx
		newCtx.Col = colIndex - 1
		newCtx.Row = rowInt - 1
		return refParsed.Evaluate(&newCtx)
	} else {
		return valText, nil
	}

}
func (f *Formula) Evaluate(ctx *Context) (interface{}, error) {
	switch {
	case f.Value != nil:
		return f.Value.Evaluate(ctx)
	case f.Body != nil:
		return f.Body.Evaluate(ctx)
	}
	return nil, errors.New(" is empty")
}
func (f *Body) Evaluate(ctx *Context) (interface{}, error) {
	switch {
	case f.Body != nil:
		return f.Body.Evaluate(ctx)
	}
	return nil, errors.New("Body is empty")
}
func (v *Value) Evaluate(ctx *Context) (interface{}, error) {
	switch {
	case v.String != nil:
		return *v.String, nil
	case v.Number != nil:
		return *v.Number, nil
	case v.Int != nil:
		return float64(*v.Int), nil
	case v.Ref != nil:
		return getFormValue(ctx, *v.Ref)
	}
	return nil, errors.New("unresolved value type")
}
func (f *Formulas) Evaluate(ctx *Context) (interface{}, error) {
	switch {
	case f.Attr != nil:
		return f.Attr.Evaluate(ctx)
	case f.Dbrw != nil:
		return f.Dbrw.Evaluate(ctx)
	case f.Subnm != nil:
		return f.Subnm.Evaluate(ctx)
	case f.If != nil:
		return f.If.Evaluate(ctx)
	case f.String != nil:
		return f.String.Evaluate(ctx)
	}
	return nil, errors.New("unsupported function")
}
func getStringsFromArgs(ctx *Context, args []*Body) ([]string, error) {
	var result []string
	for _, arg := range args {
		name, err := arg.Evaluate(ctx)
		if err != nil {
			return nil, err
		}
		if name == nil {
			result = append(result, "nil")
		} else {
			if reflect.TypeOf(name).Name() != "string" {
				result = append(result, strconv.FormatFloat(name.(float64), 'f', -1, 64))
				//return nil, errors.New("arg " + strconv.Itoa(i) + " is not string")
			} else {
				result = append(result, name.(string))
			}
		}

	}
	return result, nil
}

func (s *String) Evaluate(ctx *Context) (interface{}, error) {
	switch {
	case s.Args != nil:
		val, err := s.Args.Evaluate(ctx)
		if err != nil {
			return nil, err
		}
		return fmt.Sprintf("%v", val), nil
	}
	return nil, errors.New("missing arg")
}

func (a *Attr) Evaluate(ctx *Context) (interface{}, error) {
	// ATTR(dimensionName, dimensionValueName, attributeName)

	switch {
	case a.Args != nil && len(a.Args) == 3:
		names, err := getStringsFromArgs(ctx, a.Args)
		if err != nil {
			return nil, err
		}
		val, err := olap.GetDimension(names[0]).GetValue(names[1]).GetAttribute(names[2])
		if err != nil {
			return nil, err
		}
		return val, nil
	}
	return nil, errors.New("missing args")
}
func (d *DBRW) Evaluate(ctx *Context) (interface{}, error) {
	switch {
	case d.Args != nil:
		names, err := getStringsFromArgs(ctx, d.Args)
		if err != nil {
			return nil, err
		}
		addr := strings.Join(names[1:], ",,")
		c := olap.GetCube(names[0])
		if c == nil {
			return nil, errors.New("wrong cube name")
		}
		if ctx.UserInput != nil && ctx.UserInput[ctx.Row] != nil && ctx.UserInput[ctx.Row][ctx.Col] != nil {
			err := c.UpdateDetailed(addr, ctx.UserInput[ctx.Row][ctx.Col])
			if err != nil {
				return nil, err
			}
			return ctx.UserInput[ctx.Row][ctx.Col], nil
		}
		val, isConsolidated := c.GetValueWithAccessCheck(addr, ctx.Username)
		switch val.(type) {
		case error:
			return nil, err
		}
		ctx.isConsolidated = isConsolidated

		// if perm != "rw" {
		// 	ctx.isConsolidated = true
		// }
		//if !isConsolidated {
		//	// if perm == "none" {
		//		return nil, errors.New("нет доступа")
		//	// }
		//}
		//for i, dvName := range names[1:] {
		//	if ctx.Perms[c.Dimensions[i]] != nil && (ctx.Perms[c.Dimensions[i]][dvName] != "rw" && ctx.Perms[c.Dimensions[i]][dvName] != "r") {
		//		return nil, errors.New("нет доступа")
		//	}
		//}

		return val, nil
	}
	return nil, errors.New("empty args")
}
func (s *SUBNM) Evaluate(ctx *Context) (interface{}, error) {
	// SUBNM (dimensionName, subsetName, attrName, defaultValue, dbrwAddr)
	switch {
	case s.Args != nil:
		names, err := getStringsFromArgs(ctx, s.Args)
		if err != nil {
			return nil, err
		}
		dim := olap.GetDimension(names[0])
		if dim == nil {
			return nil, errors.New("dimension not found")
		}
		if len(names) > 3 && names[3] != "" {

			ctx.SUBNM.DimensionValues = append(ctx.SUBNM.DimensionValues, olap.DimensionValue{
				Level:      0,
				Name:       names[3],
				Parent:     nil,
				Children:   nil,
				Attributes: nil,
			})
		}
		if len(names) > 4 && names[4] != "" {
			fmt.Print("SUBNM: drwAddr: ")
			fmt.Println(names[4])
			cubesAndDim := strings.Split(names[4], ",,")
			c := olap.GetCube(cubesAndDim[0])
			if ctx.UserInput != nil && ctx.UserInput[ctx.Row] != nil && ctx.UserInput[ctx.Row][ctx.Col] != nil {
				fmt.Println("SUBNM: Update value for ", strings.Join(cubesAndDim[1:], ",,"), " with val ", ctx.UserInput[ctx.Row][ctx.Col])
				err := c.UpdateDetailed(strings.Join(cubesAndDim[1:], ",,"), ctx.UserInput[ctx.Row][ctx.Col])
				if err != nil {
					return nil, err
				}
			}
			ctx.SUBNM.IsDBRW = true
			val, _ := c.GetValueWithAccessCheck(strings.Join(cubesAndDim[1:], ",,"), ctx.Username)
			defVal := fmt.Sprintf("%v", val)
			if defVal == "0" {
				defVal = "Выберите контрагента"
			}
			ctx.SUBNM.DimensionValues = append(ctx.SUBNM.DimensionValues, olap.DimensionValue{
				Level:      0,
				Name:       defVal,
				Parent:     nil,
				Children:   nil,
				Attributes: nil,
			})
		}
		if len(names) == 1 || names[1] == "" {
			if len(names) > 2 {
				ctx.SUBNM.Attr = names[2]
			}
			for _, dV := range dim.Hierarchy {
				ctx.SUBNM.DimensionValues = append(ctx.SUBNM.DimensionValues, *dV)
			}
		} else {
			for _, s := range dim.Subsets {
				if s.Name == names[1] {
					if len(names) > 2 {
						ctx.SUBNM.Attr = names[2]
					}
					for _, dV := range s.Hierarchy {
						ctx.SUBNM.DimensionValues = append(ctx.SUBNM.DimensionValues, *dV)
					}
				}
			}
		}

		if ctx.UserInput != nil && ctx.UserInput[ctx.Row] != nil && ctx.UserInput[ctx.Row][ctx.Col] != nil {
			return ctx.UserInput[ctx.Row][ctx.Col], nil
		}
		if len(ctx.SUBNM.DimensionValues) > 0 {
			return ctx.SUBNM.DimensionValues[0].Name, nil
		} else {
			return nil, errors.New("нет доступа")
		}

	}
	return nil, errors.New("empty args")
}
