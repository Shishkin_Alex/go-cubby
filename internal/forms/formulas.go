package forms

import (
	"github.com/alecthomas/participle"
)

func ParseFormula(str string) (Formula, error) {
	parser, err := participle.Build(&Formula{})
	if err != nil {
		return Formula{}, err
	}
	ast := &Formula{}
	err = parser.ParseString(str, ast)
	//spew.Dump(ast)
	if err != nil {
		return Formula{}, err
	}
	return *ast, nil
}
