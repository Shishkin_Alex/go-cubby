package handlers

import (
	"bitbucket.org/Shishkin_Alex/go-cubby/internal/forms"
	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
)

//TODO: Username from JWT
func GetForm(c *gin.Context) {
	claims := jwt.ExtractClaims(c)
	username, _ := claims["email"].(string)

	formName, ok := c.GetQuery("name")
	_, isEdit := c.GetQuery("edit")
	if ok {
		if isEdit {
			c.JSON(200, forms.GetForm(formName))
			return
		} else {
			c.JSON(200, forms.GetFormParsed(formName, make(map[int]map[int]interface{}), username))
			return
		}
	}
	c.JSON(200, forms.Forms)
}
func GetFormWithInput(c *gin.Context) {
	claims := jwt.ExtractClaims(c)
	username, _ := claims["email"].(string)

	formName := c.Param("name")
	userInput := make(map[int]map[int]interface{})
	err := c.BindJSON(&userInput)
	if err != nil {
		c.JSON(500, err)
		return
	}
	form := forms.GetFormParsed(formName, userInput, username)
	c.JSON(200, form)
}
func AddForm(c *gin.Context) {
	var req struct {
		Name string
	}
	c.BindJSON(&req)
	f := forms.Form{
		Name: req.Name,
	}
	forms.AddForm(f)
}
func UpdateForm(c *gin.Context) {
	formName := c.Param("name")
	var req [][]forms.Cell
	c.BindJSON(&req)
	forms.GetForm(formName).Cells = req
}
func DeleteForm(c *gin.Context) {

}
