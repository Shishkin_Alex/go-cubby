package handlers

import (
	"bitbucket.org/Shishkin_Alex/go-cubby/internal/olap"
	"github.com/gin-gonic/gin"
)

func GetRules(c *gin.Context) {
	cubeName := c.Param("cube")
	cube := olap.GetCube(cubeName)
	rules, err := cube.GetRulesRaw()
	if err != nil {
		c.JSON(500, err)
		return
	}
	c.JSON(200, rules)
}
func UpdateRules(c *gin.Context) {
	cName := c.Param("cube")
	var body struct {
		Text string
	}
	err := c.BindJSON(&body)
	if err != nil {
		c.JSON(500, err)
		return
	}
	cube := olap.GetCube(cName)
	err = cube.UpdateRules(body.Text)
	if err != nil {
		c.JSON(409, err)
		return
	}
	c.Status(200)
}
