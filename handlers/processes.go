package handlers

import (
	"bitbucket.org/Shishkin_Alex/go-cubby/internal/integrator"
	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"strconv"
)

func GetProcesses(c *gin.Context) {
	list, err := integrator.GetProcessesList()
	if err != nil {
		c.Error(err)
		return
	}
	c.JSON(200, list)
}
func AddProcess(c *gin.Context) {
	var req struct {
		Name string `json:"name"`
	}
	err := c.BindJSON(&req)
	if err != nil {
		c.Error(err)
		return
	}
	err = integrator.AddProcess(req.Name)
	if err != nil {
		c.Error(err)
		return
	}
	c.Status(200)
}
func GetProcessList(c *gin.Context) {
	name := c.Param("process")
	list, err := integrator.GetProcessFileList(name)
	if err != nil {
		c.Error(err)
		return
	}
	c.JSON(200, list)
}
func GetProcessFileBody(c *gin.Context) {
	processName := c.Param("process")
	fileName := c.Param("filename")
	body, err := integrator.GetProcessFileBody(processName, fileName)
	if err != nil {
		c.Error(err)
		return
	}
	c.JSON(200, body)
}
func UpdateProcessFileBody(c *gin.Context) {
	processName := c.Param("process")
	var body struct {
		Name string
		Text string
	}
	err := c.BindJSON(&body)
	if err != nil {
		c.Error(err)
		return
	}
	err = integrator.UpdateProcessFile(processName, body.Name, body.Text)
	if err != nil {
		c.Error(err)
		return
	}
}
func AddProcessFile(c *gin.Context) {
	processName := c.Param("process")
	var body struct {
		Name string `json:"name"`
	}
	err := c.BindJSON(&body)
	if err != nil {
		c.Error(err)
		return
	}
	err = integrator.AddProcessFile(processName, body.Name)
	if err != nil {
		c.Error(err)
		return
	}
}
func DeleteProcessFile(c *gin.Context) {
	processName := c.Param("process")
	fileName := c.Param("filename")
	err := integrator.DeleteProcessFile(processName, fileName)
	if err != nil {
		c.Error(err)
		return
	}
}

func RunProcess(c *gin.Context) {
	name := c.Param("process")
	err := integrator.RunProcess(name)
	if err != nil {
		c.Error(err)
		return
	}
	c.Status(200)
}

func RunProcessWithParams(c *gin.Context) {
	name := c.Param("process")
	type procParams struct {
		Subnm map[int]map[int]string
	}
	claims := jwt.ExtractClaims(c)
	userId, _ := claims["id"]

	params := procParams{}
	err := c.BindJSON(&params)
	params.Subnm[74] = map[int]string{}
	params.Subnm[74][74] = strconv.Itoa(int(userId.(float64)))
	if err != nil {
		c.Error(err)
		return
	}
	err = integrator.RunProcessWithParams(name, params.Subnm)
	if err != nil {
		c.Error(err)
		return
	}
	c.Status(200)
}
