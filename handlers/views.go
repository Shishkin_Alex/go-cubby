package handlers

import (
	"bitbucket.org/Shishkin_Alex/go-cubby/internal/cubeview"
	"github.com/gin-gonic/gin"
)

func ListViews(c *gin.Context) {
	c.JSON(200, cubeview.Views)
}
func AddView(c *gin.Context) {
	var req cubeview.View

	err := c.BindJSON(&req)

	if err != nil {
		c.JSON(400, err)
		return
	}

	cubeview.AddView(req)
}
func GetView(c *gin.Context) {
	name := c.Param("name")

	c.JSON(200, cubeview.GetView(name))
}
func UpdateView(c *gin.Context) {
	name := c.Param("name")

	var req cubeview.View

	err := c.BindJSON(&req)

	if err != nil {
		c.JSON(400, err)
		return
	}

	req.Name = name

	cubeview.UpdateView(req)
}

func DeleteView(c *gin.Context) {
	name := c.Param("name")
	cubeview.DeleteViewByName(name)

}
