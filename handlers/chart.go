package handlers

import (
	"bitbucket.org/Shishkin_Alex/go-cubby/internal/olap"
	"github.com/gin-gonic/gin"
	"strconv"
	"strings"
)

func GetChartData(c *gin.Context) {
	cubeName := c.Param("cube")
	dimName := c.Query("dimension")
	levelStr := c.Query("level")
	restDV := c.Query("rest")
	alias := c.Query("alias")
	if dimName != "" {
		if levelStr != "" {
			level, err := strconv.Atoi(levelStr)
			if err != nil {
				c.JSON(500, err)
				return
			}
			cube := olap.GetCube(cubeName)
			dim := olap.GetDimension(dimName)
			dimList := dim.GetHierarchy(level)
			resp := make(map[string]float64)
			for _, dv := range dimList {
				val, _ := cube.GetValue(dv.Name + ",," + strings.Join(strings.Split(restDV, ","), ",,"))

				if alias != "" {
					resp[dv.Attributes["Alias"].(string)] = val.(float64)
				} else {
					resp[dv.Name] = val.(float64)
				}

			}
			c.JSON(200, resp)
			return
		}
	}
}
