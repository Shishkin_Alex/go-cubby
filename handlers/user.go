package handlers

import (
	"bitbucket.org/Shishkin_Alex/go-cubby/config"
	"bitbucket.org/Shishkin_Alex/go-cubby/internal/olap"
	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
)

func GetUsers(c *gin.Context) {
	var users []User
	err := config.DB.Model(&users).Select()
	if err != nil {
		c.AbortWithError(500, err)
	}
	c.JSON(200, users)
}

func GetCurrentUser(c *gin.Context) {

	claims := jwt.ExtractClaims(c)

	name := claims["name"].(string)

	user := User{}

	role := olap.GetUserRoleByNameAsString(name)

	user.Name = name

	user.Role = role

	c.JSON(200, user)
}
