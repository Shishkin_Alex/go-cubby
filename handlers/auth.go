package handlers

import (
	"bitbucket.org/Shishkin_Alex/go-cubby/internal/olap"
	"errors"
	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"time"
)

type User struct {
	Name     string `json:"name"`
	Password string `json:"-"`
	Role     string `json:"role"`
}

var JWT = &jwt.GinJWTMiddleware{
	Realm:       "test zone",
	Key:         []byte("is sires the best?"),
	Timeout:     time.Hour,
	MaxRefresh:  time.Hour,
	IdentityKey: "name",
	PayloadFunc: func(data interface{}) jwt.MapClaims {
		if v, ok := data.(*User); ok {
			return jwt.MapClaims{
				"name": v.Name,
				"role": v.Role,
			}
		}
		return jwt.MapClaims{}
	},
	IdentityHandler: func(c *gin.Context) interface{} {
		claims := jwt.ExtractClaims(c)
		return &User{
			Name: claims["name"].(string),
			Role: claims["role"].(string),
		}
	},
	Authenticator: Login,
	Authorizator: func(data interface{}, c *gin.Context) bool {
		if _, ok := data.(*User); ok {
			return true
		}
		return false
	},
	Unauthorized: func(c *gin.Context, code int, message string) {
		c.JSON(code, gin.H{
			"code":    code,
			"message": message,
		})
	},
	TokenLookup:   "header: Authorization, query: token, cookie: jwt",
	TokenHeadName: "Bearer",
	TimeFunc:      time.Now,
}

func Login(c *gin.Context) (interface{}, error) {
	var user struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	err := c.BindJSON(&user)

	if err != nil {
		return nil, err
	}
	var userDict User

	pass := olap.GetUserPasswordByName(user.Email)

	if !CheckPasswordHash(user.Password, pass) {
		return nil, errors.New("email or password doesn't match")
	}

	role := olap.GetUserRoleByNameAsString(user.Email)

	userDict.Name = user.Email

	userDict.Role = role

	return &userDict, nil
}
