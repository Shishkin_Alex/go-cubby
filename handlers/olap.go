package handlers

import (
	"bitbucket.org/Shishkin_Alex/go-cubby/internal/cubeview"
	"bitbucket.org/Shishkin_Alex/go-cubby/internal/forms"
	"bitbucket.org/Shishkin_Alex/go-cubby/internal/olap"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"strings"

	"github.com/gin-gonic/gin"
)

type DimensionValueNode struct {
	Name     string
	Children []DimensionValueNode
}

func FillParentLevel(parent *olap.DimensionValue, level int) {
	parent.Level = level
	if parent.Parent != nil {
		FillParentLevel(parent.Parent, level+1)
	}
}

func GetCubeview(c *gin.Context) {
	selectedDVsStr, _ := c.GetQuery("selectedDimensionValues")

	cubeName, ok := c.GetQuery("cubeName")
	if !ok {
		c.JSON(400, "missed cubeName")
		return
	}
	dimensionNameAxisXStr, ok := c.GetQuery("dimensionNameAxisX")
	if !ok {
		c.JSON(400, "missed dimensionNameAxisX")
		return
	}
	dimensionNameAxisYStr, ok := c.GetQuery("dimensionNameAxisY")
	if !ok {
		c.JSON(400, "missed dimensionNameAxisY")
		return
	}

	dimAxisY := olap.GetDimension(dimensionNameAxisYStr)
	dimAxisX := olap.GetDimension(dimensionNameAxisXStr)
	cube := olap.GetCube(cubeName)
	selectedDVsWithDim := strings.Split(selectedDVsStr, ",,")
	var selectedDVs [][]string
	selectedDVs = make([][]string, len(selectedDVsWithDim))
	for i, v := range selectedDVsWithDim {
		selectedDVs[i] = strings.Split(v, "::")
	}

	var result [][]interface{}

	for _, dimvY := range *dimAxisY.FlatHierarchy() {
		var resRow []interface{}
		for _, dimvX := range *dimAxisX.FlatHierarchy() {
			var addr []string
			for _, dimName := range cube.Dimensions {
				if dimName == dimAxisY.Name {
					addr = append(addr, dimvY.Name)
				} else if dimName == dimAxisX.Name {
					addr = append(addr, dimvX.Name)
				} else {
					for _, sdv := range selectedDVs {
						if sdv[0] == dimName {
							addr = append(addr, sdv[1])
						}
					}
				}
			}
			val, _ := cube.GetValue(strings.Join(addr, ",,"))
			resRow = append(resRow, fmt.Sprintf("%v", val))
		}
		result = append(result, resRow)
	}
	c.JSON(200, result)
}

func NodeTreeToArray(arr *[]*olap.DimensionValue, values []DimensionValueNode, parent *olap.DimensionValue) {
	for i := range values {
		dv := olap.DimensionValue{
			Level:  0,
			Name:   values[i].Name,
			Parent: parent,
		}
		*arr = append(*arr, &dv)
		NodeTreeToArray(arr, values[i].Children, &dv)
		if len(values[i].Children) == 0 && parent != nil {
			FillParentLevel(parent, 1)
		}
	}
}

func GetCube(c *gin.Context) {
	cubeName, ok := c.GetQuery("name")
	if ok {
		c.JSON(200, olap.GetCube(cubeName))
	}

	var res []*olap.Cube
	for i := range olap.Cubes {
		res = append(res, &olap.Cube{
			Name:       olap.Cubes[i].Name,
			Dimensions: olap.Cubes[i].Dimensions,
		})
	}
	c.JSON(200, res)
}
func AddCube(c *gin.Context) {
	var req struct {
		Name       string
		Dimensions []string
	}
	err := c.Bind(&req)
	if err != nil {
		c.Error(err)
		return
	}
	olap.AddCube(olap.InitCube(req.Name, req.Dimensions))
}
func DeleteCube(c *gin.Context) {
	cubeName, ok := c.GetQuery("name")
	if !ok {
		c.JSON(401, "missing cube name")
	}
	olap.DeleteCube(cubeName)
}
func GetDimension(c *gin.Context) {
	dimensionName, ok := c.GetQuery("name")
	if ok {
		c.JSON(200, olap.GetDimension(dimensionName))
		return
	}
	c.JSON(200, olap.Dimensions)
}

func AddDimension(c *gin.Context) {
	var req struct {
		Name string
		Tree []DimensionValueNode
	}
	err := c.BindJSON(&req)
	if err != nil {
		c.Error(err)
		return
	}
	var dvs []*olap.DimensionValue
	NodeTreeToArray(&dvs, req.Tree, nil)
	d := olap.Dimension{
		Name:      req.Name,
		Hierarchy: dvs,
	}
	olap.AddDimension(&d)
	c.JSON(200, d)
}
func UpdateDimension(c *gin.Context) {
	dName := c.Param("name")
	var req struct {
		Tree []DimensionValueNode
	}
	err := c.BindJSON(&req)
	if err != nil {
		c.Error(err)
	}
	var dvs []*olap.DimensionValue
	NodeTreeToArray(&dvs, req.Tree, nil)
	dim := olap.GetDimension(dName)
	dim.Hierarchy = dvs
}
func DeleteDimension(c *gin.Context) {
	dimName := c.Param("name")
	olap.DeleteDimension(dimName)
}
func AddAttribute(c *gin.Context) {
	dimName := c.Param("dimension")
	var req struct {
		Name string `json:"name"`
		Type string `json:"type"`
	}
	err := c.BindJSON(&req)
	if err != nil {
		c.Error(err)
	}
	dim := olap.GetDimension(dimName)
	dim.AddAttribute(req.Name, req.Type)
	c.JSON(200, dim)
}
func UpdateAttribute(c *gin.Context) {
	dimName := c.Param("dimension")
	var req struct {
		DvName        string      `json:"dv_name"`
		AttributeName string      `json:"attribute_name"`
		Value         interface{} `json:"value"`
	}
	err := c.BindJSON(&req)
	if err != nil {
		c.Error(err)
	}
	dim := olap.GetDimension(dimName)
	spew.Dump(dim)
	dv := dim.GetValue(req.DvName)
	dv.SetAttribute(req.AttributeName, req.Value)
	c.JSON(200, dim)
}
func GetSubset(c *gin.Context) {
	subsetName := c.Param("subset")
	dimensionName := c.Param("dimension")
	dim := olap.GetDimension(dimensionName)
	c.JSON(200, dim.GetSubset(subsetName))
}
func UpdateSubset(c *gin.Context) {
	//TODO: Update subset
}
func AddSubset(c *gin.Context) {
	dimensionName := c.Param("dimension")
	sub := olap.Subset{}
	err := c.BindJSON(&sub)
	if err != nil {
		c.JSON(500, err)
		return
	}
	fmt.Println(dimensionName, sub)
	olap.GetDimension(dimensionName).AddSubset(&sub)
	c.JSON(200, sub)
}
func DeleteSubset(c *gin.Context) {
	subsetName := c.Param("subset")
	dimensionName := c.Param("dimension")
	dim := olap.GetDimension(dimensionName)
	dim.DeleteSubset(subsetName)
}
func SaveBackup(c *gin.Context) {
	err := cubeview.SaveBackup()
	if err != nil {
		fmt.Println(err)
		c.JSON(500, err)
		return
	}
	err = forms.SaveBackup()
	if err != nil {
		fmt.Println(err)
		c.JSON(500, err)
		return
	}
	err = olap.SaveBackup()
	if err != nil {
		fmt.Println(err)
		c.JSON(500, err)
		return
	}
}
func LoadBackup(c *gin.Context) {

}
func UpdateDBRW(c *gin.Context) {
	var updDBRW struct {
		Cube            string
		DimensionsNames []string
		Value           float64
	}
	err := c.BindJSON(&updDBRW)
	if err != nil {
		c.JSON(500, err)
		return
	}
	cube := olap.GetCube(updDBRW.Cube)
	err = cube.UpdateDetailed(strings.Join(updDBRW.DimensionsNames, ",,"), updDBRW.Value)
	if err != nil {
		c.JSON(500, err)
		return
	}
}
