package config

import (
	"github.com/go-pg/pg/v9"
	"os"
)

var DB *pg.DB

func InitDB() error {
	db := pg.Connect(&pg.Options{
		User:     os.Getenv("DB_USERNAME"),
		Database: os.Getenv("DB_NAME"),
		Password: os.Getenv("DB_PASSWORD"),
	})
	DB = db
	return nil
}

//func createSchema(db *pg.DB) error {
//	for _, model := range []interface{}{(*handlers.User)(nil)} {
//		err := db.CreateTable(model, &orm.CreateTableOptions{})
//		if err != nil {
//			return err
//		}
//	}
//	return nil
//}
